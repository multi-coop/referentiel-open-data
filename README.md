# Référentiel de l'_open data_ 🇫🇷

[multi](https://multi.coop/) met à disposition une liste des organisations soumises à l'obligation d'ouverture des données en France, voulu comme un référentiel de l’_open data_ français que nous proposons de construire avec la communauté :

- ⬇️ [Télécharger](https://gitlab.com/multi-coop/referentiel-open-data/-/raw/main/data/referentiel.csv)
- 📊 [Analyser](https://multi-coop.gitlab.io/referentiel-open-data/)
- 🔎 [Explorer](https://explore.data.gouv.fr/?url=https://gitlab.com/multi-coop/referentiel-open-data/-/raw/main/data/referentiel-copie-explo-2.csv)

Ce travail s'appuie actuellement sur l'[Observatoire _open data_ des territoires](https://observatoire-opendata.fr/) porté par OpenDataFrance, sur la [base Sirene](https://www.sirene.fr/) de l'INSEE, ainsi que sur la [Banatic](https://www.banatic.interieur.gouv.fr/) de la direction générale des collectivités locales (voir [sources de données](#sources-de-donn%C3%A9es)).

## Méthodologie

Le [Code des relations entre le public et l’administration](https://www.legifrance.gouv.fr/codes/id/LEGISCTA000031367685/) (CRPA) définit que les administrations de l'Etat, les collectivités territoriales ainsi que les organisations chargées d’une mission de service public ont l'obligation d'ouvrir leurs données (_open data_), sauf celles qui emploient moins de 50 équivalents temps plein (ETP) ainsi que les collectivités territoriales de moins de 3 500 habitants ([plus d'informations](https://guides.etalab.gouv.fr/juridique/ouverture/)).

Le périmètre de l'Etat et des collectivités territoriales au sens du CRPA est relativement bien défini, si l'on se réfère aux [catégories juridiques](https://www.insee.fr/fr/information/2028129) de la base Sirene.

En revanche, faute de recensement des organisations "chargées d'une mission de service public" au sens du CRPA, nous avons supposé que certaines catégories juridiques correspondaient à des organisations qui entraient dans cette définition (voir [périmètre du référentiel](#p%C3%A9rim%C3%A8tre-du-r%C3%A9f%C3%A9rentiel)). Ainsi, les différentes catégories d'intercommunalité correspondent aux établissements auxquels les collectivités ou l'Etat transfèrent des compétences (en matière d'assainissement ou de transports par exemple). Cependant, des exceptions plus ou moins nombreuses à ce principe existent certainement.

### ⚠️ Limites actuelles

- Seule la population associée à des organisations intercommunales est dans le fichier (source Banatic). [Amélioration prévue](https://gitlab.com/multi-coop/referentiel-open-data/-/issues/2) : utiliser les données de recensement de l'INSEE pour les collectivités territoriales.
- Le fichier ne contient que des organisations des "territoires". Amélioration prévue : ajouter toutes les administrations centrales de l'Etat.

## Génération du jeu de données

Vous pouvez utiliser le code pour re-générer le jeu de données :

1. Cloner le projet
2. Installer les dépendences (nécessite poetry) : `poetry install`
3. Lancer le script de génération : `poetry run python scripts/generation_referentiel.py`

## Sources de données

Liste des sources de données utilisées pour créer le référentiel :

| Nom              | Description                                                  | Source                                                                                                                                               |
| ---------------- | ------------------------------------------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------- |
| SIRENE           | Registre des entreprises et de leur établissements.          | data.gouv.fr : [StockUniteLegale_utf8.zip](https://www.data.gouv.fr/fr/datasets/base-sirene-des-entreprises-et-de-leurs-etablissements-siren-siret/) |
| BANATIC          | Base nationale sur l'intercommunalité                        | Banatic : [Liste des groupements - France entière.xls](https://www.banatic.interieur.gouv.fr/V5/fichiers-en-telechargement/fichiers-telech.php)      |
| Observatoire ODF | Jeu de données des organismes publiant leur jeux de données. | [Airtable](https://airtable.com/shrKrV6KY7BlhHDx7)                                                                                                   |

La clé utilisée pour agréger les données est le numéro SIREN des organisations ("unités légales").

## Traitements

Explication simplifiée des traitements effectués sur les données sources pour créer le référentiel :

- Récupération des organisations présentes dans la base Sirene
  - Stockage du numéro SIREN, de la dénomination, de la [catégorie juridique](https://www.insee.fr/fr/information/2028129) et de la [tranche d'effectif](https://sirene.fr/sirene/public/variable/trancheEffectifsUniteLegale)
- Récupération des organisations présentes dans la Banatic
  - Stockage du numéro SIREN et de la population
- Récupération des organisations présenntes dans le fichier ODF
  - Stockage du numéro SIREN
- Suppression des organisations dont la catégorie juridique est en dehors du [périmètre](#périmètre-du-référentiel)
- Suppression des organisations ayant une tranche d'effectif en-dessous de 21 (50 ETP)
- Suppression des organisations ayant une population inférieure à 3 500 habitants

## Schéma

Structure du fichier référentiel :

- `siren` (chaîne de caractère) : numéro SIREN de l'organisation
- `denomination` (chaîne de caractère) : nom de l'organisation
- `categorie_juridique` (chaîne de caractère) : [catégorie juridique](https://www.insee.fr/fr/information/2028129) de l'organisation
- `tranche_effectif` (chaîne de caractère) : [tranche d'effectif](https://sirene.fr/sirene/public/variable/trancheEffectifsUniteLegale) de l'organisation
- `population` (entier) : population couverte par l'organisation
- `present_fichier_odf` (booléen) : si l'organisation est présente dans le fichier Observatoire ODF
- `obligation_publication` (chaîne de caractère) :
  - `non_connu` si la tranche d'effectif ou la population n'est pas disponible
  - `oui` si la tranche d'effectif est supérieure ou égale à 21 (50 ETP) ou si la population est supérieure à 3 500 habitants

## Périmètre du référentiel

La liste des [catégories juridiques](https://www.insee.fr/fr/information/2028129) incluses dans le référentiel :

| Code | Libellé                                                                                           |
| ---- | ------------------------------------------------------------------------------------------------- |
| 4110 | Établissement public national à caractère industriel ou commercial doté d’un comptable public     |
| 4120 | Établissement public national à caractère industriel ou commercial non doté d’un comptable public |
| 4140 | Établissement public local à caractère industriel                                                 |
| 4150 | Régie d’une collectivité locale à caractère industriel ou commercial                              |
| 7172 | Service déconcentré de l’État à compétence (inter) départementale                                 |
| 7210 | Commune et commune nouvelle                                                                       |
| 7220 | Département                                                                                       |
| 7225 | Collectivité et territoire d&apos;Outre Mer                                                       |
| 7229 | (Autre) Collectivité territoriale                                                                 |
| 7230 | Région                                                                                            |
| 7340 | Pôle métropolitain                                                                                |
| 7343 | Communauté urbaine                                                                                |
| 7344 | Métropole                                                                                         |
| 7345 | Syndicat intercommunal à vocation multiple (SIVOM)                                                |
| 7346 | Communauté de communes                                                                            |
| 7348 | Communauté d&apos;agglomération                                                                   |
| 7353 | Syndicat intercommunal à vocation unique (SIVU)                                                   |
| 7354 | Syndicat mixte fermé                                                                              |
| 7355 | Syndicat mixte ouvert                                                                             |
| 7357 | Pôle d&apos;équilibre territorial et rural (PETR)                                                 |
| 7361 | Centre communal d’action sociale                                                                  |
| 7372 | Service départemental d’incendie et de secours (SDIS)                                             |
| 7379 | (Autre) Établissement public administratif local                                                  |
