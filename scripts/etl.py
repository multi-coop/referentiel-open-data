import os
from abc import ABC

import numpy as np
import pandas as pd
import unidecode
from pyairtable import Table

CATEGORIES_JURIDIQUE_OD = [
    "7172",
    "7210",
    "7220",
    "7229",
    "7230",
    "7340",
    "7343",
    "7344",
    "7345",
    "7346",
    "7348",
    "7353",
    "7354",
    "7355",
    "7357",
    "7361",
    "4120",
    "4110",
    "4140",
    "7379",
    "4150",
    "7372",
]


def standardisation_nom(column):
    column = column.strip().lower()
    column = column.replace("'", "_").replace(" ", "_")
    column = unidecode.unidecode(column)  # Get rid of accents
    return column


class DataSet(ABC):
    """
    Définit un datatset et ses functions d'ETL (extract, transform and load)
    Tous les datasets doivent être à la maille 'commune', en sortie de la
    méthode transform
    Chaque ligne représente une unique commune.
    """

    def __init__(self, name, cache=True):
        self.name = name
        self.df = None

        # ETL
        self.extract(cache)
        self.transform()
        print(f"Dataset {self.name} : {len(self.df)} lignes après filtrage")

    def extract(self, cache):
        self.df = pd.read_csv(
            f"raw_data/{self.name}.csv",
            sep=";",
            dtype={"siren": str, "ndeg_siren": str, "N° SIREN": str},
        )
        print(f"Dataset {self.name} : {len(self.df)} lignes lues")

    def transform(self):
        self.df = self.df.rename(standardisation_nom, axis="columns")
        self.filtre_colonnes()

    @property
    def data(self):
        return self.df


class Banatic(DataSet):
    def __init__(self, cache, *args, **kwargs):
        super().__init__("banatic", cache, *args, **kwargs)

    def extract(self, cache):
        if not cache:
            self.df = pd.read_csv(
                "https://www.banatic.interieur.gouv.fr/V5/fichiers-en-"
                "telechargement/telecharger.php?zone=N&date=01/01/2023&format=A",
                encoding="latin_1",
                sep="\t",
                index_col=False,
                dtype={"N° SIREN": str},
            )
            self.df.to_csv(f"raw_data/{self.name}.csv", sep=";", index=False)
        else:
            super().extract(cache)

    def transform(self):
        super().transform()
        self.df.population = self.df.population.astype("int", errors="ignore")

    def filtre_colonnes(self):
        colonnes = ["ndeg_siren", "population"]
        self.df = self.df[colonnes]
        self.df = self.df.rename(columns={"ndeg_siren": "siren"})


class SIRENE(DataSet):
    def __init__(self, cache=True, *args, **kwargs):
        super().__init__("sirene", cache, *args, **kwargs)

    def extract(self, cache=True):
        if not cache:
            chunksize = 10000
            first = True
            for i, chunk in enumerate(
                pd.read_csv(
                    "https://files.data.gouv.fr/insee-sirene/StockUniteLegale"
                    "_utf8.zip",
                    compression="zip",
                    chunksize=chunksize,
                    index_col=False,
                    usecols=[
                        "etatAdministratifUniteLegale",
                        "siren",
                        "denominationUniteLegale",
                        "trancheEffectifsUniteLegale",
                        "categorieJuridiqueUniteLegale",
                    ],
                    dtype={
                        "siren": str,
                        "trancheEffectifsUniteLegale": str,
                        "etatAdministratifUniteLegale": str,
                        "denominationUniteLegale": str,
                        "categorieJuridiqueUniteLegale": str,
                    },
                )
            ):
                en_activite = chunk[chunk["etatAdministratifUniteLegale"] == "A"]

                en_activite = en_activite.replace("NN", np.nan)

                en_activite = en_activite[
                    [
                        "siren",
                        "denominationUniteLegale",
                        "trancheEffectifsUniteLegale",
                        "categorieJuridiqueUniteLegale",
                    ]
                ]

                en_activite = en_activite[
                    en_activite.categorieJuridiqueUniteLegale.isin(
                        CATEGORIES_JURIDIQUE_OD
                    )
                ]

                if first:
                    en_activite.to_csv(
                        f"raw_data/{self.name}.csv", sep=";", index=False
                    )
                    first = False
                else:
                    en_activite.to_csv(
                        f"raw_data/{self.name}.csv",
                        sep=";",
                        index=False,
                        header=False,
                        mode="a",
                    )

        super().extract(cache)

    def transform(self):
        super().transform()
        self.df.categorie_juridique = self.df.categorie_juridique.astype("str")
        self.df.categorie_juridique = self.df.categorie_juridique.apply(
            lambda x: x.replace(".0", "")
        )

    def filtre_colonnes(self):
        colonnes = [
            "siren",
            "denominationunitelegale",
            "categoriejuridiqueunitelegale",
            "trancheeffectifsunitelegale",
        ]
        self.df = self.df[colonnes]
        self.df = self.df.rename(
            columns={
                "denominationunitelegale": "denomination",
                "categoriejuridiqueunitelegale": "categorie_juridique",
                "trancheeffectifsunitelegale": "tranche_effectif",
            }
        )


class ODF(DataSet):
    def __init__(self, cache, *args, **kwargs):
        super().__init__("odf", cache, *args, **kwargs)

    def extract(self, cache):
        if not cache:
            # Send the request to the API
            table = Table(
                os.getenv("AIRTABLE_TOKEN"),
                os.getenv("AIRTABLE_BASE_ID"),
                "Organisations",
            )
            convert = []
            for line in table.all():
                convert.append(line["fields"])
            self.df = pd.DataFrame(convert)
            self.df.to_csv(f"raw_data/{self.name}.csv", sep=";", index=False)
        else:
            super().extract(cache)

    def transform(self):
        super().transform()

    def filtre_colonnes(self):
        colonnes = ["siren"]
        self.df = self.df[colonnes]
