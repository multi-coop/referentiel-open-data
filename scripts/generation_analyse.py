import pandas as pd
from ydata_profiling import ProfileReport

df = pd.read_csv('data/referentiel.csv', sep=';')
profile = ProfileReport(df, title="Pandas Profiling Report")
profile.to_file("index.html")
