import math
import numpy as np
import pandas as pd
from dotenv import load_dotenv

import etl

CACHE = True
load_dotenv()


def contrainte_publication(row):
    if row['tranche_effectif'] < 20 or 0 < row['population'] < 3500:
        return 'non'
    if np.isnan(row['tranche_effectif']):
        return 'non_connu'
    elif row['population'] == -1:
        return 'non_connu'
    elif row['tranche_effectif'] >= 20 and row['population'] >= 3500:
        return 'oui'
    else:
        return 'non'


df = etl.SIRENE(cache=CACHE).data

# Ajout des données de population pour les structures referencees
# dans la BANATIC
population = etl.Banatic(cache=CACHE).data
df = pd.merge(df, population, how="left", on="siren")
df.population = df.population.fillna(-1)

# Creation colonne obligation legale de publication pour les structures publiques
# ayant au moins 50 agents et plus de 3500 citoyens
df["obligation_publication"] = df.apply(contrainte_publication, axis=1)

# Création colonne pour les collectivités ayant déjà publié en open-data
odf = list(etl.ODF(cache=CACHE).data.siren)
df["present_fichier_odf"] = False
df["present_fichier_odf"] = df.siren.apply(lambda x: True if str(x) in odf else False)

# Suppression des structures dont il est sur qu'elles ne sont pas soumises à l'obligation
df = df[df.obligation_publication != "non"]

# Mise en forme des données
df.population = df.population.astype("int")
df.tranche_effectif = df.tranche_effectif.fillna("non_connu")
df.tranche_effectif = df.tranche_effectif.astype("str").apply(
    lambda x: x.replace(".0", ""))

# Sauvegarde du référentiel
df[["siren", "denomination", "categorie_juridique", "tranche_effectif", "population",
    "present_fichier_odf", "obligation_publication", ]].to_csv("data/referentiel.csv",
                                                               sep=";", index=False)
